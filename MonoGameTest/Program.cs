﻿using System;

namespace MonoGameTest
{
    public class Program
    {
        
        [STAThread]
        static void Main()
        {
            using (var game = new Game1())
                game.Run();
        }
    }
}